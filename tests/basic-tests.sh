set -e

# Sometimes given that operators take a bit more to start reconciling properly or simply take a bit more to properly reconsile an object
# this function will try to make the test assertion 100 times and if at the end we do not obtain the expected behaviour that's because something is wrong
function try-to-execute () {
    sleep 1
    for run in {1..100}; do
        RESULT=$(eval $1)
        if test "$RESULT" == "$2" ; then
            return 0
        fi
    done
    echo "Command "$1" failed to run more than 100 times"  >&2
    exit 1
}

echo Creating a project and verifying all the necessary things
oc create -f tests/resources/mysite.yaml

echo Check if the project was created with the name of the CR created
try-to-execute "oc get project mysite -o json | jq -r '.metadata.name'" "mysite"
echo -e "OK\n"

echo Check if the project was created with the proper displayname
try-to-execute "oc get project mysite -o json | jq -r '.metadata.annotations[\"openshift.io/display-name\"]'" "mysite"
echo -e "OK\n"

echo Check if the project was created with the proper category
try-to-execute "oc get project mysite -o json | jq -r '.metadata.labels[\"cern.ch/webservices-category\"]'" "personal"
echo -e "OK\n"

echo Description related tests
echo Check if the project was created with the proper description
try-to-execute "oc get project mysite -o json | jq -r '.metadata.annotations[\"openshift.io/description\"]'" "This site will host my personal webpage"
echo -e "OK\n"

echo Update site description and check if the annotation was updated
oc patch paassite.webservices.cern.ch mysite -p '{"spec":{"siteDescription":"This new description is wayy funnier"}}' --type='merge'
try-to-execute "oc get project mysite -o json | jq -r '.metadata.annotations[\"openshift.io/description\"]'" "This new description is wayy funnier"
echo -e "OK\n"

echo Owner related tests
echo Check if the project was created with the proper requester
try-to-execute "oc get project mysite -o json | jq -r '.metadata.annotations[\"openshift.io/requester\"]'" "estevesm"
echo -e "OK\n"

echo Check if the project was created with the proper owner
try-to-execute "oc get project mysite -o json | jq -r '.metadata.labels[\"cern.ch/webservices-owner\"]'" "estevesm"
echo -e "OK\n"

echo Check if the roleBinding admin was created with the proper owner
try-to-execute "oc get rolebinding admin -n mysite -o json | jq -r '.subjects[0].name'" "estevesm"
echo -e "OK\n"

echo Updating CR owner
oc patch paassite.webservices.cern.ch mysite -p '{"spec":{"owner":"bob"}}' --type='merge'
echo Check if the project requester was updated
try-to-execute "oc get project mysite -o json | jq -r '.metadata.annotations[\"openshift.io/requester\"]'" "bob"
echo -e "OK\n"

echo Check if the project was owner was updated
try-to-execute "oc get project mysite -o json | jq -r '.metadata.labels[\"cern.ch/webservices-owner\"]'" "bob"
echo -e "OK\n"

echo Check if the roleBinding admin was update to the new owner
try-to-execute "oc get rolebinding admin -n mysite -o json | jq -r '.subjects[0].name'" "bob"
echo -e "OK\n"

echo Resource quota related tests
echo Check if the project has the proper quota label
try-to-execute "oc get project mysite -o json | jq -r '.metadata.labels[\"cern.ch/webservices-quota\"]'" "small"
echo -e "OK\n"

echo Check if the resource quota was created and has proper values
try-to-execute "oc get resourcequota mysite-quota -n mysite -o json | jq -r '.metadata.name'" "mysite-quota"
try-to-execute "oc get resourcequota mysite-quota -n mysite -o json | jq -r '.spec.hard.cpu'" "1"
echo -e "OK\n"

echo Updating CR quota
oc patch paassite.webservices.cern.ch mysite -p '{"spec":{"quotaProfile":"large"}}' --type='merge'
echo Check if the project quota was updated
try-to-execute "oc get project mysite -o json | jq -r '.metadata.labels[\"cern.ch/webservices-quota\"]'" "large"
echo -e "OK\n"

echo Check if the resource quota was updated with proper values
try-to-execute "oc get resourcequota mysite-quota -n mysite -o json | jq -r '.metadata.name'" "mysite-quota"
try-to-execute "oc get resourcequota mysite-quota -n mysite -o json | jq -r '.spec.hard.cpu'" "4"
echo -e "OK\n"


oc delete -f tests/resources/mysite.yaml

# echo Check if the project was removed after removing the CR
# try-to-execute "oc get project mysite -o json" "Error from server (NotFound): namespaces \"mysite\" not found"
# echo -e "OK\n"

