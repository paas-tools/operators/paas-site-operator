package utils

// Contains checks if a string is in an array
func Contains(list []string, s string) bool {
	for _, v := range list {
		if v == s {
			return true
		}
	}
	return false
}

// Remove removes a string from an array of strings
func Remove(list []string, s string) []string {
	for i, v := range list {
		if v == s {
			list = append(list[:i], list[i+1:]...)
		}
	}
	return list
}

// CorrectPair checks if dictionary contains a key or if it has the proper value set
func CorrectPair(key string, value string, dic map[string]string) bool {
	if dic[key] == "" || dic[key] != value {
		return false
	} 
	return true
}