package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// To understand how the different enumerations are declared visit
// https://blog.learngoprogramming.com/golang-const-type-enums-iota-bc4befd096d3

//InternetVisibility Visibility of the website
type InternetVisibility string
const (
	// UnknownIV unknown state pattern to be sure of enum’s initialization.
	UnknownIV InternetVisibility = "unknown"
	// Always site is fully visible to the Internet
	Always InternetVisibility = "always"
	// Never site is never visible to the Internet
	Never InternetVisibility = "never"
	// Mixed this is manual mode
	Mixed InternetVisibility = "mixed"
)

// QuotaProfile is the quota profile of the website.
//
// Quota is set directly in Project's ResourceQuota
type QuotaProfile string
const (
	// UnknownQP unknown state pattern to be sure of enum’s initialization.
	UnknownQP QuotaProfile = "unknown"
	// Small
	Small QuotaProfile = "small"
	// Medium
	Medium QuotaProfile = "medium"
	// Large
	Large QuotaProfile = "large"
	// Custom means this is not managed by the controller
	Custom QuotaProfile = "custom"
)

// PodDefaultResourcesProfile can be interperted as site type
//
// Defines what we put in Project's LimitRange and sets the pod
// requests/limits when not specified explicitly by user in pod spec.
type PodDefaultResourcesProfile string
const (
	// UnknownRP unknown state pattern to be sure of enum’s initialization.
	UnknownRP PodDefaultResourcesProfile = "unknown"
	Static PodDefaultResourcesProfile = "static"
	Dynamic PodDefaultResourcesProfile = "dynamic"
	NodeJs PodDefaultResourcesProfile = "nodeJs"
)

// SiteCategory defines the site's lifecycle
type SiteCategory string
const (
	// UnknownSC unknown state pattern to be sure of enum’s initialization.
	UnknownSC SiteCategory = "unknown"
	Official SiteCategory = "official"
	Test SiteCategory = "test"
	Personal SiteCategory = "personal"
)

//Status return code of the last operation
type Status string
const (
	// UnknownS unknown state pattern to be sure of enum’s initialization.
	UnknownS Status = "unknown"
	NotProvisioned Status = "notProvisioned"
	Provisioned Status = "provisioned"
	ProvisionedWithError Status = "provisionedWithError"
)

//BlockedStatus contain the status if the site was blocked
type BlockedStatus string
const (
	// UnknownBS unknown state pattern to be sure of enum’s initialization.
	UnknownBS BlockedStatus = "unknown"
	Blocked BlockedStatus = "blocked"
	NotBlocked BlockedStatus = "notBlocked"
	BlockingError BlockedStatus = "blockingError"
)

// PaasSiteSpec defines the desired state of PaasSite
// +k8s:openapi-gen=true
type PaasSiteSpec struct {
	// Fully qualified dns name
	Fqdn []string `json:"fqdn"`

	// Owner of the website
	Owner string `json:"owner"`

	// Description of the website
	SiteDescription string `json:"siteDescription"`

	// If a site was blocked by the network team
	Blocked bool `json:"blocked"`

	// Visibility of the website
	InternetVisibility InternetVisibility `json:"internetVisibility"`

	// Visibility of the website
	QuotaProfile QuotaProfile `json:"quotaProfile"`

	// Site type
	PodDefaultResourcesProfile PodDefaultResourcesProfile `json:"podDefaultResourcesProfile"`

	// Defines the site's lifecycle
	SiteCategory SiteCategory `json:"siteCategory"`
}

// PaasSiteStatus defines the observed state of PaasSite
// +k8s:openapi-gen=true
type PaasSiteStatus struct {
	// Return code of the last operation
	Status Status `json:"status"`

	// Error message in case of error
	ErrorMessage string `json:"errorMessage"`

	// Current owner of the website
	CurrentOwner string `json:"currentOwner"`

	// Current fqdn
	CurrentFqdn []string `json:"currentFqdn"`

	// Current block status
	BlockedStatus BlockedStatus `json:"blockedStatus"`

	// Date of the last change done by the owner in the
	// site's namespace (so we know if a site is still being maintained).
	// Needs to be recalculated on a regular basis (watch changes on secondary resources?)...
	// Can we really define this for an openshift site?
	// If not, better not set this at all.
	LastModificationDate string `json:"lastModificationDate"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// PaasSite is the Schema for the paassites API
// +k8s:openapi-gen=true
// +kubebuilder:subresource:status
type PaasSite struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   PaasSiteSpec   `json:"spec,omitempty"`
	Status PaasSiteStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// PaasSiteList contains a list of PaasSite
type PaasSiteList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []PaasSite `json:"items"`
}

func init() {
	SchemeBuilder.Register(&PaasSite{}, &PaasSiteList{})
}
