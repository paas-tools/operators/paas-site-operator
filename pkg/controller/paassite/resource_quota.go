package paassite

import (
	"context"

	"github.com/go-logr/logr"
	webservicesv1alpha1 "gitlab.cern.ch/paas-tools/paas-site-operator/pkg/apis/webservices/v1alpha1"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	goCli "sigs.k8s.io/controller-runtime/pkg/client"
)

var flavors = map[webservicesv1alpha1.QuotaProfile]v1.ResourceList{
	webservicesv1alpha1.Small: v1.ResourceList{
		v1.ResourceCPU:                    resource.MustParse("1"),
		v1.ResourceMemory:                 resource.MustParse("2Gi"), // respect 2GB/CPU ratio in Openstack VMs
		v1.ResourcePods:                   resource.MustParse("8"),   // respect 2GB/CPU ratio in Openstack VMs
		v1.ResourceServices:               resource.MustParse("8"),
		v1.ResourceRequestsStorage:        resource.MustParse("10Gi"),
		v1.ResourcePersistentVolumeClaims: resource.MustParse("5"),
	},

	webservicesv1alpha1.Medium: v1.ResourceList{
		v1.ResourceCPU:                    resource.MustParse("2"),
		v1.ResourceMemory:                 resource.MustParse("4Gi"),
		v1.ResourcePods:                   resource.MustParse("16"),
		v1.ResourceServices:               resource.MustParse("16"),
		v1.ResourceRequestsStorage:        resource.MustParse("40Gi"),
		v1.ResourcePersistentVolumeClaims: resource.MustParse("10"),
	},

	webservicesv1alpha1.Large: v1.ResourceList{
		v1.ResourceCPU:                    resource.MustParse("4"),
		v1.ResourceMemory:                 resource.MustParse("8Gi"),
		v1.ResourcePods:                   resource.MustParse("32"),
		v1.ResourceServices:               resource.MustParse("32"),
		v1.ResourceRequestsStorage:        resource.MustParse("100Gi"),
		v1.ResourcePersistentVolumeClaims: resource.MustParse("15"),
	},
}

func newResourceQuota(instance *webservicesv1alpha1.PaasSite) *v1.ResourceQuota {
	labels := map[string]string{
		"app":     instance.Name,
		"version": "v0.1",
	}
	return &v1.ResourceQuota{
		ObjectMeta: metav1.ObjectMeta{
			Name:      instance.Name + "-quota",
			Namespace: instance.Name,
			Labels:    labels,
		},
		Spec: v1.ResourceQuotaSpec{
			Hard: flavors[instance.Spec.QuotaProfile],
		},
	}
}

func createOrUpdateResourceQuota(client goCli.Client, reqLogger logr.Logger, instance *webservicesv1alpha1.PaasSite) error {
	if instance.Spec.QuotaProfile == webservicesv1alpha1.Custom {
		return nil
	}

	resourceQuota := newResourceQuota(instance)
	log.Info("RESOURCE QUOTA")
	log.Info(resourceQuota.String())
	if err := client.Create(context.TODO(), resourceQuota); err != nil {
		if !errors.IsAlreadyExists(err) {
			reqLogger.Error(err, "Failed to create resource quota", "resourceQuota.name", resourceQuota.Name)
			return err
		}

		if err = client.Update(context.TODO(), resourceQuota); err != nil {
			reqLogger.Error(err, "Failed to update project quota, resourceQuota", "resourceQuota.name", resourceQuota.Name)
			return err
		}
	}

	return nil
}
