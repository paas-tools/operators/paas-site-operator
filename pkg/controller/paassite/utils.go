package paassite

import(
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// Update label of Object
func updateLabel(object *metav1.ObjectMeta, key string, value string){
	labels := object.GetLabels()
	labels[key] = value
	object.SetLabels(labels)
}

// Update annotation of Object
func updateAnnotation(object *metav1.ObjectMeta, key string, value string){
	annotations := object.GetAnnotations()
	annotations[key] = value
	object.SetAnnotations(annotations)
}