package paassite

import(
	"context"

	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	v1 "k8s.io/api/rbac/v1"
	webservicesv1alpha1 "gitlab.cern.ch/paas-tools/paas-site-operator/pkg/apis/webservices/v1alpha1"
	goCli "sigs.k8s.io/controller-runtime/pkg/client"


)

func newAdminRoleBinding(instance *webservicesv1alpha1.PaasSite) *v1.RoleBinding {
	labels := map[string]string{
		"app":     instance.Name,
		"version": "v0.1",
	}
	return &v1.RoleBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name: "admin",
			Namespace: instance.Name,
			Labels: labels,
		},
		RoleRef: v1.RoleRef{
			Kind: "ClusterRole",
			Name: "admin",
		},
		Subjects: []v1.Subject{
			v1.Subject{
				Kind: "User",
				Name: instance.Spec.Owner,
			},
		},
		
	}
}

func createOrUpdateRoleBinding(client goCli.Client, reqLogger logr.Logger, instance *webservicesv1alpha1.PaasSite) error {
	roleBinding := newAdminRoleBinding(instance)
	if err := client.Create(context.TODO(), roleBinding); err != nil {
		if !errors.IsAlreadyExists(err) {
			reqLogger.Error(err, "Failed to create role binding", "roleBinding.name", roleBinding.Name)
			return err
		}

		if err = client.Update(context.TODO(), roleBinding); err != nil {
			reqLogger.Error(err, "Failed to update project owner, roleBinding", "roleBinding.name", roleBinding.Name)
			return err
		}
	}
	return nil
}