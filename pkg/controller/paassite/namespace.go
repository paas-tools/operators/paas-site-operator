package paassite

import(
	"context"

	"github.com/go-logr/logr"
	corev1 "k8s.io/api/core/v1"
	goCli "sigs.k8s.io/controller-runtime/pkg/client"
	"k8s.io/apimachinery/pkg/types"


)

func updateNamespaceOwner(client goCli.Client, reqLogger logr.Logger,  namespaceName string, newOwner string) error {
	namespace, err := getNamespace(client, reqLogger, namespaceName)
	if err != nil {
		return err
	}

	updateAnnotation(&namespace.ObjectMeta, RequesterAnnotation, newOwner)
	updateLabel(&namespace.ObjectMeta, OwnerLabel, newOwner)

	if err := updateNamespace(client, reqLogger, namespace); err != nil {
		reqLogger.Error(err, "Failed to update namespace owner", "project.name", namespace.Name)
		return err
	}
	return nil
}

func getNamespace(client goCli.Client, reqLogger logr.Logger, namespaceName string) (namespace *corev1.Namespace, err error) {
	namespace = &corev1.Namespace{}
	key := types.NamespacedName{
		Name: namespaceName,
	}
	if err = client.Get(context.TODO(), key, namespace); err != nil {
		reqLogger.Error(err, "Failed to get a namespace", "namespace.name", namespaceName)
		return namespace, err
	}
	return namespace, nil
}

func updateNamespace(client goCli.Client, reqLogger logr.Logger, namespace *corev1.Namespace) (err error) {
	if err = client.Update(context.TODO(), namespace); err != nil {
		return err
	}
	return nil
}