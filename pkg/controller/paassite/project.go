package paassite

import(
	"context"
	"os"


	"github.com/go-logr/logr"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	v1 "github.com/openshift/api/project/v1"
	webservicesv1alpha1 "gitlab.cern.ch/paas-tools/paas-site-operator/pkg/apis/webservices/v1alpha1"
	goCli "sigs.k8s.io/controller-runtime/pkg/client"
	"k8s.io/apimachinery/pkg/types"


)

//DefaultNodeSelector default node selector to be assigned to each project
var DefaultNodeSelector string = os.Getenv("DEFAULT_NODE_SELECTOR")

// newProjectForCR returns a project with the same name/namespace as the cr
func newProjectForCR(instance *webservicesv1alpha1.PaasSite) *v1.Project {
	annotations := map[string]string{
		DisplaynameAnnotation: instance.Name,
		NodeSelectorAnnotation: DefaultNodeSelector,
	}
	labels := map[string]string{
		"app":     instance.Name,
		"version": "v0.1",
		CategoryLabel: string(instance.Spec.SiteCategory),
	}
	return &v1.Project{
		ObjectMeta: metav1.ObjectMeta{
			Name:         instance.Name,
			GenerateName: instance.Name + "-project",
			Annotations: annotations,
			Labels:       labels,
		},
	}
}

func getProject(client goCli.Client, reqLogger logr.Logger, projectName string) (proj *v1.Project, err error) {
	proj = &v1.Project{}
	key := types.NamespacedName{
		Name: projectName,
	}
	if err = client.Get(context.TODO(), key, proj); err != nil {
		return proj, err
	}
	return proj, nil
}

func updateProject(client goCli.Client, reqLogger logr.Logger, proj *v1.Project) (err error) {
	if err = client.Update(context.TODO(), proj); err != nil {
		return err
	}
	return nil
}

func deleteProject(client goCli.Client, reqLogger logr.Logger, projectName string) (err error) {
	project, err := getProject(client, reqLogger, projectName)
	if err != nil{
		return err
	}

	if err = client.Delete(context.TODO(), project); err != nil {
		return err
	}
	return nil
}










