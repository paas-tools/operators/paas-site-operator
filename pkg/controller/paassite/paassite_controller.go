package paassite

import (
	"context"

	"github.com/go-logr/logr"
	webservicesv1alpha1 "gitlab.cern.ch/paas-tools/paas-site-operator/pkg/apis/webservices/v1alpha1"
	utils "gitlab.cern.ch/paas-tools/paas-site-operator/pkg/utils"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	goCli "sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const (
	//DescriptionAnnotation annotation key for openshift project description
	DescriptionAnnotation = "openshift.io/description"

	//DisplaynameAnnotation annotation key for openshift project display name
	DisplaynameAnnotation = "openshift.io/display-name"

	//RequesterAnnotation annotation key for openshift project requester
	RequesterAnnotation = "openshift.io/requester"

	//NodeSelectorAnnotation annotation key for openshift node-selector
	NodeSelectorAnnotation = "openshift.io/node-selector"

	//OwnerLabel custom CERN label key for project owner
	OwnerLabel = "cern.ch/webservices-owner"

	//CategoryLabel custom CERN label key for project category
	CategoryLabel = "cern.ch/webservices-category"

	//QuotaLabel custom CERN label key for project quota
	QuotaLabel = "cern.ch/webservices-quota"

	paasSiteFinalizer = "finalizer.controller-paassite.webservices.cern.ch"
)

var log = logf.Log.WithName("controller_paassite")

// Add creates a new PaasSite Controller and adds it to the Manager. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcilePaasSite{client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("paassite-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to primary resource PaasSite
	err = c.Watch(&source.Kind{Type: &webservicesv1alpha1.PaasSite{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	// Types the controller creates that are owned by the primary resource
	// TODO might need to add stuff here
	return nil
}

// blank assignment to verify that ReconcilePaasSite implements reconcile.Reconciler
var _ reconcile.Reconciler = &ReconcilePaasSite{}

// ReconcilePaasSite reconciles a PaasSite object
type ReconcilePaasSite struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	client goCli.Client
	scheme *runtime.Scheme
}

// Reconcile reads that state of the cluster for a PaasSite object and makes changes based on the state read
// and what is in the PaasSite.Spec
// Note:
// The Controller will requeue the Request to be processed again if the returned error is non-nil or
// Result.Requeue is true, otherwise upon completion it will remove the work from the queue.
func (r *ReconcilePaasSite) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	reqLogger := log.WithValues("Request.Namespace", request.Namespace, "Request.Name", request.Name)
	reqLogger.Info("Reconciling PaasSite")

	// Fetch the PaasSite instance
	instance := &webservicesv1alpha1.PaasSite{}
	if err := r.client.Get(context.TODO(), request.NamespacedName, instance); err != nil {
		if errors.IsNotFound(err) {
			// Request object not found, could have been deleted after reconcile request.
			// Owned objects are automatically garbage collected. For additional cleanup logic use finalizers.
			// Return and don't requeue
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	// Check if the CR was marked to be deleted
	if instance.GetDeletionTimestamp() != nil {
		if err := RemoveFinalizer(r.client, reqLogger, instance); err != nil {
			return reconcile.Result{}, err
		}
		return reconcile.Result{}, nil
	}

	if err := AddFinalizer(r.client, reqLogger, instance); err != nil {
		return reconcile.Result{}, err
	}

	// Check if there's already a project, if not, create it
	if instance.Status.Status == webservicesv1alpha1.UnknownS || instance.Status.Status == webservicesv1alpha1.NotProvisioned {
		if err := createProject(r.client, reqLogger, instance); err != nil {
			updateCRwithError(r.client, reqLogger, instance, err.Error())
			return reconcile.Result{}, err
		}
		reqLogger.Info("Created new Project")
		return reconcile.Result{Requeue: true}, nil
	}

	proj, err := getProject(r.client, reqLogger, instance.Name)
	if err != nil {
		if errors.IsNotFound(err) {
			// Projects take a bit to be created until there reconsile without sending errors
			// of not found
			return reconcile.Result{Requeue: true}, nil
		}
		return reconcile.Result{}, err
	}

	annotations := proj.GetAnnotations()
	// Update project description
	if !utils.CorrectPair(DescriptionAnnotation, instance.Spec.SiteDescription, annotations) {
		if err := updateDescription(r.client, reqLogger, instance); err != nil {
			updateCRwithError(r.client, reqLogger, instance, err.Error())
			return reconcile.Result{}, err
		}
		reqLogger.Info("Updated project description")
		return reconcile.Result{Requeue: true}, nil
	}

	// Update project owner and requester
	if instance.Spec.Owner != instance.Status.CurrentOwner {
		if err := updateOwner(r.client, reqLogger, instance); err != nil {
			updateCRwithError(r.client, reqLogger, instance, err.Error())
			return reconcile.Result{}, err
		}
		reqLogger.Info("Updated project owner")
		return reconcile.Result{Requeue: true}, nil
	}

	labels := proj.GetAnnotations()
	// Update project quota
	if !utils.CorrectPair(QuotaLabel, string(instance.Spec.QuotaProfile), labels) {
		if err := updateQuotaProfile(r.client, reqLogger, instance); err != nil {
			updateCRwithError(r.client, reqLogger, instance, err.Error())
			return reconcile.Result{}, err
		}
		reqLogger.Info("Updated project quota")
		return reconcile.Result{Requeue: true}, nil
	}

	// Update CR Status to Sucessfull Reconsile
	if instance.Status.Status != webservicesv1alpha1.Provisioned {
		successfulReconcile(r.client, reqLogger, instance)
	}

	reqLogger.Info("PaasSite reconciled", "Name", instance.Name)
	return reconcile.Result{}, nil
}

// RemoveFinalizer will execute the necessary actions to clean up resources created by
// the existance of the CR and finally will remove the finalizer from PAASSite CR so
// that it can be deleted
func RemoveFinalizer(client goCli.Client, reqLogger logr.Logger, instance *webservicesv1alpha1.PaasSite) error {
	if utils.Contains(instance.GetFinalizers(), paasSiteFinalizer) {
		//Here we ignore erros of not found since we are trying to cleanup things
		//TODO this might not be necessary since the resource is namespaced
		if err := deleteProject(client, reqLogger, instance.Name); err != nil && !errors.IsNotFound(err) {
			return err
		}

		// Remove paasSiteFinalizer. Once all finalizers have been
		// removed, the object will be deleted.
		instance.SetFinalizers(utils.Remove(instance.GetFinalizers(), paasSiteFinalizer))
		err := client.Update(context.TODO(), instance)
		if err != nil {
			reqLogger.Error(err, "Failed to update a CR when removing the finalizer")
			return err
		}
	}
	return nil
}

// AddFinalizer will add a finalizer to the PAASSite CR so that
// we can delete it appropriately
func AddFinalizer(client goCli.Client, reqLogger logr.Logger, instance *webservicesv1alpha1.PaasSite) error {

	if utils.Contains(instance.GetFinalizers(), paasSiteFinalizer) {
		return nil
	}

	reqLogger.Info("Adding Finalizer to the PaasSite")
	instance.SetFinalizers(append(instance.GetFinalizers(), paasSiteFinalizer))

	err := client.Update(context.TODO(), instance)
	if err != nil {
		reqLogger.Error(err, "Failed to update a CR with a finalizer")
		return err
	}

	// Initialize the Status of the CR
	instance.Status = webservicesv1alpha1.PaasSiteStatus{
		Status:               webservicesv1alpha1.UnknownS,
		CurrentOwner:         "Unknown",
		ErrorMessage:         "",
		CurrentFqdn:          make([]string, 0),
		BlockedStatus:        webservicesv1alpha1.UnknownBS,
		LastModificationDate: "Unknown",
	}

	if err := client.Status().Update(context.TODO(), instance); err != nil {
		reqLogger.Error(err, "Failed to update PAASSite status on createProject", "paassite.name", instance.Name)
		return err
	}

	return nil
}

func createProject(client goCli.Client, reqLogger logr.Logger, instance *webservicesv1alpha1.PaasSite) error {
	// Create a project object
	project := newProjectForCR(instance)

	// TODO might want/need to add owner ref

	// Create the project resource
	if err := client.Create(context.TODO(), project); err != nil {
		reqLogger.Error(err, "Failed to create project", "project.name", project.Name)
		// TODO might want to update the status of the CR to Not provisioned or provisoned with error
		return err
	}

	// Update the Status of the CR
	instance.Status.Status = webservicesv1alpha1.Provisioned
	if err := client.Status().Update(context.TODO(), instance); err != nil {
		reqLogger.Error(err, "Failed to update PAASSite status on createProject", "paassite.name", instance.Name)
		return err
	}

	return nil
}

func updateDescription(client goCli.Client, reqLogger logr.Logger, instance *webservicesv1alpha1.PaasSite) error {
	//Get project to be updated
	project, err := getProject(client, reqLogger, instance.Name)
	if err != nil {
		return err
	}

	//Update the description annotation
	updateAnnotation(&project.ObjectMeta, DescriptionAnnotation, instance.Spec.SiteDescription)

	//Update the project
	if err := updateProject(client, reqLogger, project); err != nil {
		reqLogger.Error(err, "Failed to update project description", "project.name", project.Name)
		return err
	}
	return nil
}

func updateOwner(client goCli.Client, reqLogger logr.Logger, instance *webservicesv1alpha1.PaasSite) error {

	//To update the owner of a project we have to update the namespace instead of the project, if we try to update
	//the owner label or the requester annotation we will get a imutable resource error message
	if err := updateNamespaceOwner(client, reqLogger, instance.Name, instance.Spec.Owner); err != nil {
		return err
	}

	// Try and create a new admin RoleBinding for the project if one aready exists update it with
	// the new owner instead
	if err := createOrUpdateRoleBinding(client, reqLogger, instance); err != nil {
		return err
	}

	instance.Status.CurrentOwner = instance.Spec.Owner
	if err := client.Status().Update(context.TODO(), instance); err != nil {
		reqLogger.Error(err, "Failed to update PAASSite status (currentOwner)", "paassite.name", instance.Name)
		return err
	}

	return nil
}

func updateCRwithError(client goCli.Client, reqLogger logr.Logger, instance *webservicesv1alpha1.PaasSite, errorMessage string) {
	instance.Status.Status = webservicesv1alpha1.ProvisionedWithError
	instance.Status.ErrorMessage = errorMessage
	if err := client.Status().Update(context.TODO(), instance); err != nil {
		reqLogger.Error(err, "Failed to update PAASSite with error message", "error message", errorMessage)
	}
}

func successfulReconcile(client goCli.Client, reqLogger logr.Logger, instance *webservicesv1alpha1.PaasSite) {
	instance.Status.Status = webservicesv1alpha1.Provisioned
	instance.Status.ErrorMessage = ""
	if err := client.Status().Update(context.TODO(), instance); err != nil {
		reqLogger.Error(err, "Failed to update PAASSite that reconsiled with no problems")
	}
}

func updateQuotaProfile(client goCli.Client, reqLogger logr.Logger, instance *webservicesv1alpha1.PaasSite) error {
	// Try and create a new resource quota for the project if one aready exists update it with
	// the new quota values instead
	err := createOrUpdateResourceQuota(client, reqLogger, instance)
	if err != nil {
		return err
	}

	//Get project to be updated
	namespace, err := getNamespace(client, reqLogger, instance.Name)
	if err != nil {
		return err
	}

	//Update the quota label
	updateLabel(&namespace.ObjectMeta, QuotaLabel, string(instance.Spec.QuotaProfile))

	//Update the project
	if err := updateNamespace(client, reqLogger, namespace); err != nil {
		reqLogger.Error(err, "Failed to update project quota", "project.name", namespace.Name)
		return err
	}
	return nil
}
