package controller

import (
	"gitlab.cern.ch/paas-tools/paas-site-operator/pkg/controller/paassite"
)

func init() {
	// AddToManagerFuncs is a list of functions to create controllers and add them to a manager.
	AddToManagerFuncs = append(AddToManagerFuncs, paassite.Add)
}
