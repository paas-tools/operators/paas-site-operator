set -e

oc create -f deploy/crds/webservices_v1alpha1_paassite_crd.yaml
oc create -f deploy/service_account.yaml
oc create -f deploy/role_binding.yaml
oc create -f deploy/operator.yaml