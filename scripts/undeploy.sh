set -e
oc delete paassites  --all
oc delete -f deploy/crds/webservices_v1alpha1_paassite_crd.yaml
oc delete -f deploy/service_account.yaml
oc delete -f deploy/role_binding.yaml
oc delete -f deploy/operator.yaml